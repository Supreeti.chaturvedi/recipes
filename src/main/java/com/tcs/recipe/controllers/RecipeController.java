package com.tcs.recipe.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.services.RecipeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class RecipeController {
	private final RecipeService recipeService;

	public RecipeController(RecipeService recipeService) {
		super();
		this.recipeService = recipeService;
	}
	
	@RequestMapping({"/recipe/{id}/show"})
	public String showById(@PathVariable String id, Model model) {
		model.addAttribute("recipe", recipeService.findById(new Long(id))); 
		
		return "recipe/show";
	}
	
	@RequestMapping({"recipe/new"})
	public String newRecipe(Model model) {
		model.addAttribute("recipe", new Recipe()); 
		
		return "recipe/recipeform";
	}
	
	@RequestMapping({"recipe/{id}/update"})
	public String updateRecipe(@PathVariable String id, Model model) {
		model.addAttribute("recipe", recipeService.findById(Long.valueOf(id))); 
		
		return "recipe/recipeform";
	}

	//@RequestMapping(name="recipe", method = RequestMethod.POST)
	@PostMapping
	@RequestMapping("recipe")
	public String saveOrUpdate(@ModelAttribute Recipe recipe) {
		Recipe saved = recipeService.saveRecipe(recipe);
		
		return "redirect:/recipe/" + saved.getId() + "/show";
	}
	
	@GetMapping
	@RequestMapping({"recipe/{id}/delete"})
	public String deleteById(@PathVariable String id) {
		log.info("deleting ID :: " + id);
		
		recipeService.deleteById(Long.valueOf(id));
		
		return "redirect:/";
	}
	
	@GetMapping
	@RequestMapping({"/recipeSearch/"})
	public String searchRecipe(@RequestParam(name = "title") String title, Model model) {
		log.info("searching for recipe name :: " + title);
		Recipe recipe = recipeService.findByName(title);
		
		log.info("recipe found from findByName :: " + recipe);
		
		String returnValue = "";
		
		if(recipe!=null) {
			log.info("recipe not null");
			model.addAttribute("recipe", recipe);
			returnValue = "redirect:/recipe/" + recipe.getId() + "/show";
		}
		else {
			log.info("recipe null");
			model.addAttribute("recipe", recipeService.getRecipes()); 
			returnValue = "redirect:/index";
		}
		
		return returnValue;
	}
}