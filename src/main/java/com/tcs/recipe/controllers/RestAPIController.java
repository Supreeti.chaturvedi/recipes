package com.tcs.recipe.controllers;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.tcs.recipe.model.Ingredient;
import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.services.ImageService;
import com.tcs.recipe.services.IngredientService;
import com.tcs.recipe.services.RecipeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value="Recipe Application", tags ="Operations pertaining to recipe in Recipe Application")
@RestController
@RequestMapping("/api")
public class RestAPIController {
	public static final Logger logger = LoggerFactory.getLogger(RestAPIController.class);
	
	RecipeService recipeService;
	IngredientService ingredientService;
	ImageService imageService;
	
	public RestAPIController(RecipeService recipeService, IngredientService ingredientService, ImageService imageService) {
		super();
		this.recipeService = recipeService;
		this.ingredientService = ingredientService;
		this.imageService = imageService;
	}
	
	  @ApiOperation(value = "Get Recipe")
	    @ApiResponses(
	            value = {
	            		@ApiResponse(code = 204, message = "No Recipe Found"),
	                    @ApiResponse(code = 200, message = "Successful get Recipe Operation")
	            }
	    )
	
	@RequestMapping(value = "/recipe", method= RequestMethod.GET)
	public ResponseEntity<Set<Recipe>> getRecipe(){
		Set<Recipe> recipes = recipeService.getRecipes();
		
		if(recipes==null || (recipes!=null && recipes.isEmpty())) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Set<Recipe>>(recipes, HttpStatus.OK);
	}
	
	  @ApiOperation(value = "Show Recipe By Id")
	    @ApiResponses(
	            value = {
	            		@ApiResponse(code = 204, message = "No Recipe Found by id"),
	                    @ApiResponse(code = 200, message = "Success! Recipe found by Id")
	            }
	    )
	@RequestMapping(value = "/recipe/id/{id}", method= RequestMethod.GET)
	public ResponseEntity<Recipe> showRecipeById(@PathVariable("id") long id){
		Recipe recipe = recipeService.findById(id);
		
		if(recipe == null) {
			return new ResponseEntity<Recipe>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Recipe>(recipe, HttpStatus.OK);
	}
	
	  @ApiOperation(value = "Show Recipe By Name")
	    @ApiResponses(
	            value = {
	                    @ApiResponse(code = 204, message = "No Recipe Found by Name"),
	                    @ApiResponse(code = 200, message = "Success! Recipe found by name")
	            }
	    )
	  
	@RequestMapping(value = "/recipe/name/{name}", method= RequestMethod.GET)
	public ResponseEntity<Recipe> showRecipeByName(@PathVariable("name") String name){
		Recipe recipe = recipeService.findByName(name);
		
		if(recipe == null) {
			return new ResponseEntity<Recipe>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Recipe>(recipe, HttpStatus.OK);
	}
	
	  @ApiOperation(value = "Create Recipe")
	    @ApiResponses(
	            value = {
	                    @ApiResponse(code = 409, message = "Conflict in Creating Recipe"),
	                    @ApiResponse(code = 201, message = "Success! Recipe Created")
	            }
	    )
	  
	@RequestMapping(value = "/recipe/", method= RequestMethod.POST)
	public ResponseEntity<?> createRecipe(@RequestBody Recipe recipe, UriComponentsBuilder ucBuilder){
		logger.info("creating recipe :: " + recipe);
		
		if(recipeService.isRecipeExist(recipe)) {
			logger.error("Recipe already exists :: ", recipe.getTitle());
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		
		recipeService.saveRecipe(recipe);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/recipe/{id}").buildAndExpand(recipe.getId()).toUri());
		
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	

	  @ApiOperation(value = "Update Recipe")
	    @ApiResponses(
	            value = {
	            		@ApiResponse(code = 404, message = "Recipe Not Found"),
	                    @ApiResponse(code = 200, message = "Success! Updated Recipe ")
	            }
	    )
	@RequestMapping(value = "/recipe/{id}", method= RequestMethod.PUT)
	public ResponseEntity<?> updateRecipe(@PathVariable("id") long id, @RequestBody Recipe recipe){
		logger.info("updating recipe with id :: " + id);
		
		Recipe currentRecipe = recipeService.findById(id);
		
		if(currentRecipe == null) {
			logger.error("Recipe not found. Unable to update recipe with ID :: " + id);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		currentRecipe.setTitle(recipe.getTitle());
		currentRecipe.setDateOfCreation(recipe.getDateOfCreation());
		currentRecipe.setDescription(recipe.getDescription());
		currentRecipe.setDirections(recipe.getDirections());
		currentRecipe.setIngredients(recipe.getIngredients());
		currentRecipe.setNoOfPeopleServing(recipe.getNoOfPeopleServing());
		currentRecipe.setVeg(recipe.isVeg());
		currentRecipe.setImage(recipe.getImage());
		
		recipeService.saveRecipe(currentRecipe);
		
		return new ResponseEntity<Recipe>(currentRecipe, HttpStatus.OK);
	}
	
	  @ApiOperation(value = "Delete Recipe")
	    @ApiResponses(
	            value = {
	                    @ApiResponse(code = 404, message = "Recipe Not Found"),
	                    @ApiResponse(code = 204, message = "Success! Deleted Recipe ")
	            }
	    )
	@RequestMapping(value = "/recipe/{id}", method= RequestMethod.DELETE)
	public ResponseEntity<?> deleteRecipeById(@PathVariable("id") long id){
		Recipe recipe = recipeService.findById(id);
		
		if(recipe == null) {
			logger.error("Recipe with id {} not found ", id);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		recipeService.deleteById(id);
		return new ResponseEntity<Recipe>(HttpStatus.NO_CONTENT);
	}
	
	
	  @ApiOperation(value = "Show Specific Recipe Specific Ingredient")
	    @ApiResponses(
	            value = {
	            		@ApiResponse(code = 404, message = "Ingredient Not Found"),
	                    @ApiResponse(code = 200, message = "Success! Specific Ingredient for Recipe shown")
	            }
	    )
	@RequestMapping(value = "/recipe/{recipeId}/ingredient/{ingredientId}", method= RequestMethod.GET)
	public ResponseEntity<Ingredient> showSpecificRecipeSpecificIngredient(@PathVariable("recipeId") long recipeId, @PathVariable("ingredientId") long ingredientId){
		Ingredient ingredient = ingredientService.findByRecipeIdAndIngredientId(recipeId, ingredientId);
		
		if(ingredient == null) {
			logger.error("ingredient with id {} not found ", ingredientId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Ingredient>(ingredient, HttpStatus.OK);
	}
	  
	  
	  @ApiOperation(value = "create Ingredient")
	    @ApiResponses(
	            value = {
	                    @ApiResponse(code = 400, message = "Bad Request"),
	                    @ApiResponse(code = 409, message = "Conflict in Creating Ingredient"),
	                    @ApiResponse(code = 201, message = "Success! Ingredient Created ")
	            }
	    )
	@RequestMapping(value = "/ingredient/", method= RequestMethod.POST)
	public ResponseEntity<?> createIngredient(@RequestBody Ingredient ingredient, UriComponentsBuilder ucBuilder){
		logger.info("creating ingredient :: " + ingredient);
		
		if(ingredient.getRecipe()==null) {
			logger.error("Ingredient does not have corresponding recipe to it :: ", ingredient.getName());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		if(ingredientService.isIngredientExist(ingredient)) {
			logger.error("Ingredient already exists :: ", ingredient.getName());
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		
		ingredientService.saveIngredient(ingredient);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/ingredient/{id}").buildAndExpand(ingredient.getId()).toUri());
		
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	  @ApiOperation(value = "Update Ingredient")
	    @ApiResponses(
	            value = {
	                    @ApiResponse(code = 404, message = "Ingredient Not Found"),
	                    @ApiResponse(code = 200, message = "Success! Ingredient Updated ")
	            }
	    )
	@RequestMapping(value = "/recipe/{recipeid}/ingredient/{ingredientid}", method= RequestMethod.PUT)
	public ResponseEntity<?> updateIngredient(@PathVariable("recipeid") long recipeId, @PathVariable("ingredientid") long ingredientId, @RequestBody Ingredient ingredient){
		logger.info("updating recipe with id :: " + recipeId + " :: and ingredient ID :: " + ingredientId);
		
		Ingredient currentIngredient = ingredientService.findByRecipeIdAndIngredientId(recipeId, ingredientId);
		
		if(currentIngredient == null) {
			logger.error("Ingredient for the recipe not found. Unable to update recipe with ID :: " + recipeId + " and Ingredient with ID :: " + ingredientId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		currentIngredient.setName(ingredient.getName());
		currentIngredient.setDesc(ingredient.getDesc());
		currentIngredient.setPortion(ingredient.getPortion());
		currentIngredient.setRecipe(ingredient.getRecipe());
		
		ingredientService.saveIngredient(currentIngredient);
		
		return new ResponseEntity<Ingredient>(currentIngredient, HttpStatus.OK);
	}
	
	  @ApiOperation(value = "Delete Ingredient")
	    @ApiResponses(
	            value = {
	                    @ApiResponse(code = 404, message = "Ingredient Not Found"),
	                    @ApiResponse(code = 200, message = "Success! Ingredient Deleted ")
	            }
	    )
	
	@RequestMapping(value = "/recipe/{recipeId}/ingredient/{ingredientId}", method= RequestMethod.DELETE)
	public ResponseEntity<Ingredient> deleteIngredient(@PathVariable("recipeId") long recipeId, @PathVariable("ingredientId") long ingredientId){
		Ingredient ingredient = ingredientService.findByRecipeIdAndIngredientId(recipeId, ingredientId);
		
		if(ingredient == null) {
			logger.error("ingredient with id {} not found ", ingredientId);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		ingredientService.deleteById(recipeId, ingredientId);
		return new ResponseEntity<Ingredient>(ingredient, HttpStatus.OK);
	}
}