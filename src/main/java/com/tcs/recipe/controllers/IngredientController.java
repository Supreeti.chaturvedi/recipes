package com.tcs.recipe.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tcs.recipe.model.Ingredient;
import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.services.IngredientService;
import com.tcs.recipe.services.RecipeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class IngredientController {
	private final RecipeService recipeService;
	private final IngredientService ingredientService;
	
	public IngredientController(RecipeService recipeService, IngredientService ingredientService) {
		super();
		this.recipeService = recipeService;
		this.ingredientService = ingredientService;
	}
	
	@RequestMapping({"/recipe/{recipeId}/ingredients"})
	public String listIngredients(@PathVariable String recipeId, Model model) {
		log.info("Getting ingredients for Recipe ID :: " + recipeId);
		Recipe recipe = recipeService.findById(Long.valueOf(recipeId));
		
		model.addAttribute("recipe", recipe); 
		
		return "recipe/ingredient/list";	
	}
	
	@RequestMapping("recipe/{recipeId}/ingredient/{id}/show")
    public String showRecipeIngredient(@PathVariable String recipeId,
                                       @PathVariable String id, Model model){
        model.addAttribute("ingredient", ingredientService.findByRecipeIdAndIngredientId(Long.valueOf(recipeId), Long.valueOf(id)));
        return "recipe/ingredient/show";
    }
	
	@GetMapping("recipe/{recipeId}/ingredient/new")
    public String newRecipe(@PathVariable String recipeId, Model model){

        //make sure we have a good id value
        Recipe recipe = recipeService.findById(Long.valueOf(recipeId));
        //todo raise exception if null

        //need to return back parent id for hidden form property
        Ingredient ingredient = new Ingredient();
        ingredient.setRecipe(recipe);
        model.addAttribute("ingredient", ingredient);

        return "recipe/ingredient/ingredientform";
    }

    @GetMapping("recipe/{recipeId}/ingredient/{id}/update")
    public String updateRecipeIngredient(@PathVariable String recipeId,
                                         @PathVariable String id, Model model){
        model.addAttribute("ingredient", ingredientService.findByRecipeIdAndIngredientId(Long.valueOf(recipeId), Long.valueOf(id)));

        return "recipe/ingredient/ingredientform";
    }

    @PostMapping("recipe/{recipeId}/ingredient")
    public String saveOrUpdate(@PathVariable String recipeId, @ModelAttribute Ingredient ingredient){
    	log.info("recipe ID being passed :: " + recipeId);
        Ingredient saved = ingredientService.saveIngredient(ingredient, Long.valueOf(recipeId).longValue());

        log.info("saved receipe id:" + recipeId);
        log.info("saved ingredient id:" + saved.getId());

        return "redirect:/recipe/" + recipeId + "/ingredient/" + saved.getId() + "/show";
    }

    @GetMapping("recipe/{recipeId}/ingredient/{id}/delete")
    public String deleteIngredient(@PathVariable String recipeId,
                                   @PathVariable String id){

        log.debug("deleting ingredient id:" + id);
        ingredientService.deleteById(Long.valueOf(recipeId), Long.valueOf(id));

        return "redirect:/recipe/" + recipeId + "/ingredients";
    }
}