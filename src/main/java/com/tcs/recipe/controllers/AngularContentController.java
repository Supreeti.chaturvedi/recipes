package com.tcs.recipe.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tcs.recipe.services.RecipeService;

@Controller
public class AngularContentController {
	private final RecipeService recipeService;

	public AngularContentController(RecipeService recipeService) {
		super();
		this.recipeService = recipeService;
	}
	
	@RequestMapping({"/spa", "/angular"})
	public String getLandingPage(Model model) {
		model.addAttribute("recipes", recipeService.getRecipes());
		return "angularIndex";
	}
}
