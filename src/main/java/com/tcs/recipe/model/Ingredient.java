package com.tcs.recipe.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Ingredient {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long Id;
	
	private String name;
	private String desc;
	private String portion;
	
	@ManyToOne
	@JsonIgnore
	private Recipe recipe;
	
	public Ingredient() {}
	
	public Ingredient(String name, String desc, String portion, Recipe recipe) {
		super();
		this.name = name;
		this.desc = desc;
		this.portion = portion;
		this.recipe = recipe;
	}
	
	public Ingredient(String name, String desc, String portion) {
		super();
		this.name = name;
		this.desc = desc;
		this.portion = portion;
	}

	public Long getId() {
		return Id;
	}
	
	public void setId(Long id) {
		Id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getPortion() {
		return portion;
	}
	
	public void setPortion(String portion) {
		this.portion = portion;
	}
	
	public Recipe getRecipe() {
		return recipe;
	}
	
	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
}