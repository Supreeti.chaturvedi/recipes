package com.tcs.recipe.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

@Entity
public class Recipe {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Timestamp dateOfCreation;
	private Integer noOfPeopleServing;
	private String description;
	private String title;
	private boolean isVeg;
	
	@Lob
	private String directions;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="recipe")
	private Set<Ingredient> ingredients = new HashSet<>();
	
	@Lob
	private Byte[] image;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Timestamp getDateOfCreation() {
		return dateOfCreation;
	}
	
	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}
	
	public Integer getNoOfPeopleServing() {
		return noOfPeopleServing;
	}
	
	public void setNoOfPeopleServing(Integer noOfPeopleServing) {
		this.noOfPeopleServing = noOfPeopleServing;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String desciption) {
		this.description = desciption;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDirections() {
		return directions;
	}

	public void setDirections(String directions) {
		this.directions = directions;
	}

	public Set<Ingredient> getIngredients() {
		return ingredients;
	}
	
	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public Byte[] getImage() {
		return image;
	}
	
	public void setImage(Byte[] image) {
		this.image = image;
	}
	
	public Recipe addIngredient(Ingredient ingredient) {
		ingredient.setRecipe(this);
		this.ingredients.add(ingredient);
		return this;
	}

	public boolean isVeg() {
		return isVeg;
	}

	public void setVeg(boolean isVeg) {
		this.isVeg = isVeg;
	}
}