package com.tcs.recipe.services;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.repositories.RecipeRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RecipeServiceImpl implements RecipeService {
	private final RecipeRepository recipeRepository;
	
	public RecipeServiceImpl(RecipeRepository recipeRepository) {
		super();
		this.recipeRepository = recipeRepository;
	}

	@Override
	public Set<Recipe> getRecipes() {
		Set<Recipe> recipeSet = new HashSet<>(); 
		
		recipeRepository.findAll().iterator().forEachRemaining(recipeSet::add);
		return recipeSet;
	}

	@Override
	public Recipe findById(Long l) {
		Optional<Recipe> recipeOptional = recipeRepository.findById(l);
		
		if(!recipeOptional.isPresent()) {
			throw new RuntimeException("Recipe Not Found !!");
		}
		
		return recipeOptional.get();
	}
	
	@Override
	public Recipe findByName(String name) {
		Set<Recipe> recipeSet = new HashSet<>(); 
		recipeRepository.findAll().iterator().forEachRemaining(recipeSet::add);
		
		Optional<Recipe> recipeOptional = recipeSet.stream().
			    filter(recipe -> recipe.getTitle().equals(name)).
			    findFirst();
		
		Recipe recipe = null;
		
		if(recipeOptional.isPresent()) {
			recipe = recipeOptional.get();
		}
		
		return recipe;
	}
	
	@Override
	public boolean isRecipeExist(Recipe recipe) {
		return findByName(recipe.getTitle()) != null;
	}
	
	@Override
	public void deleteById(Long l) {
		recipeRepository.deleteById(l);
	}

	@Override
	@Transactional
	public Recipe saveRecipe(Recipe recipe) {
		Recipe savedRecipe = recipeRepository.save(recipe);
		log.info("Saved Recipe ID :: " + savedRecipe.getId());
		
		savedRecipe.getIngredients().stream().forEach(elem -> elem.setRecipe(savedRecipe)); 
		
		return savedRecipe;
	}
}