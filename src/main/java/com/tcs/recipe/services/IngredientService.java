package com.tcs.recipe.services;

import com.tcs.recipe.model.Ingredient;

public interface IngredientService {

    Ingredient findByRecipeIdAndIngredientId(Long recipeId, Long ingredientId);
    
    Ingredient findByName(Ingredient ingredient);

    Ingredient saveIngredient(Ingredient ingredient);
    
    Ingredient saveIngredient(Ingredient ingredient, long recipeId);

    void deleteById(Long recipeId, Long idToDelete);
    
    boolean isIngredientExist(Ingredient ingredient);
}
